#include "menu.h"

#include "raylib.h"

#include "Utility/utility_menu.h"
#include "Utility/utility_calc.h"
#include "Config/audio.h"
#include "Config/visuals.h"
#include "Config/key_bindings.h"
#include "Scenes/Menu/Credits/menu_credits.h"
#include "Scenes/Menu/Settings/menu_settings.h"
#include "Scenes/Gameloop/gameloop.h"

using namespace endless::config::audio;
using namespace endless::config::visuals;
using namespace endless::utility::menu;

namespace endless
{
	namespace menu
	{
		namespace menu_main
		{
			float letterSize;
			int titleSize;

			Rectangle textBoxPlay;

			Rectangle textBoxSettings;

			Rectangle textBoxCredits;

			Rectangle textBoxExit;

			void Init()
			{
				config::visuals::menu::Init();
				config::audio::InitMenu();

				letterSize = utility::calc::NumToFloat(screenWidth) * 0.045f;
				titleSize = utility::calc::NumToInt(letterSize * 4);

				textBoxPlay = InitTextbox(letterSize, 4, 5, 5);
				textBoxSettings = InitTextbox(letterSize, 8, 5, 6.25f);
				textBoxCredits = InitTextbox(letterSize, 7.25f, 5, 7.5f);
				textBoxExit = InitTextbox(letterSize, 3.5f, 5, 8.75f);

				PlayMusicStream(music::menu);
			}

			void DrawOptions()
			{
				using namespace endless::config::visuals::menu;

				//draw play option
				DrawTextbox(menuFont, "Play", textBoxPlay, letterSize, DARKGRAY, GRAY);

				//draw settings option
				DrawTextbox(menuFont, "Settings", textBoxSettings, letterSize, DARKGRAY, GRAY);

				//draw credits option
				DrawTextbox(menuFont, "Credits", textBoxCredits, letterSize, DARKGRAY, GRAY);

				//draw exit option
				DrawTextbox(menuFont, "Exit", textBoxExit, letterSize, DARKGRAY, GRAY);
			}

			void Draw()
			{
				using namespace config::visuals::menu;

				BeginDrawing();

				//draw title
				DrawTextOnly(menuFont, "Endless", titleSize, GRAY, 5, 0);

				//draw play, settings and exit options
				DrawOptions();

				DrawGameVer(menuFont, letterSize * 0.75f, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void MainMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					//Input | Update
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						if (OptionClicked(textBoxPlay))
						{
							PlaySound(sounds::selectOption);
							config::visuals::menu::DeInit();
							config::audio::DeInitMenu();
							gameloop::Gameloop();
							Init();
						}
						else if (OptionClicked(textBoxSettings))
						{
							PlaySound(sounds::selectOption);
							menu_settings::SettingsMenu();
							Init();
						}
						else if (OptionClicked(textBoxCredits))
						{
							PlaySound(sounds::selectOption);
							menu_credits::CreditsMenu();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));
			}
		}
	}
}