#include "menu_credits.h"

#include "raylib.h"

#include "Utility/utility_menu.h"
#include "Config/audio.h"
#include "Config/visuals.h"
#include "Config/key_bindings.h"

using namespace endless::utility;
using namespace endless::config::audio;
using namespace endless::config::visuals;
using namespace endless::utility;

namespace endless
{
	namespace menu
	{
		namespace menu_credits
		{
			float letterSize;
			float letterSpacing;
			float linkSize;
			float linkSpacing;

			Rectangle textBoxExit;

			void Init()
			{
				letterSize = screenWidth * 0.025f;
				letterSpacing = letterSize / 10;
				linkSize = letterSize * 0.75f;
				linkSpacing = linkSize / 10;

				textBoxExit = utility::menu::InitTextbox(letterSize * 2, 3.5f, 5, 9.1f);
			}

			void DrawCredits()
			{
				using namespace config::visuals::menu;
				using namespace utility::menu;

				//draw library credits
				DrawCredit(menuFont, "Library used: RayLib", letterSize, GRAY, 1);

				//draw graphics motor credits
				DrawCredit(menuFont, "Graphic Motor used: OpenGL", letterSize, GRAY, 1.75f);

				//draw sound editor credits
				DrawCredit(menuFont, "Sound Recorder used: Audacity", letterSize, GRAY, 2.5f);

				//draw start of music credits
				DrawCredit(menuFont, "Music used:", letterSize, GRAY, 3.25f);

				//draw SciFi credits
				DrawCredit(menuFont, "'Sci Fi' (menu), by Bensound", letterSize, GRAY, 3.8f);

				//draw SciFi download link
				DrawCredit(linkFont, "(downloaded in https://www.bensound.com/royalty-free-music/track/sci-fi)", linkSize, GRAY, 4.1f);

				//draw SlowMo credits
				DrawCredit(menuFont, "'Slow Motion' (gameplay), by Bensound", letterSize, GRAY, 4.45f);

				//draw SlowMo download link
				DrawCredit(linkFont, "(downloaded in https://www.bensound.com/royalty-free-music/track/slow-motion)", linkSize, GRAY, 4.75f);

				//draw start of visuals credits
				DrawCredit(menuFont, "Sprites and Images used:", letterSize, GRAY, 5.5f);

				//draw StarrySky credits
				DrawCredit(menuFont, "'Starry Night Sky' (background), by kjpargeter", letterSize, GRAY, 5.9f);

				//draw StarrySky download link
				DrawCredit(linkFont, "(downloaded in https://www.freepik.com/photos/star)", linkSize, GRAY, 6.2f);

				//draw San credits
				DrawCredit(menuFont, "'San' (player), by blueapollo", letterSize, GRAY, 6.5f);

				//draw San download link
				DrawCredit(linkFont, "(downloaded in https://blueapollo.itch.io/wisp-battler)", linkSize, GRAY, 6.75f);

				//draw OBSTACLES credits
				DrawCredit(menuFont, "Obstacles by CDmir", letterSize, GRAY, 7.1f);

				//draw OBSTACLES download link
				DrawCredit(linkFont, "(downloaded in https://opengameart.org/content/asteroids-game-sprites-atlas)", linkSize, GRAY, 7.4f);

				//draw GEMS credits
				DrawCredit(menuFont, "'Gems' (pickups), by Clint Bellanger", letterSize, GRAY, 7.7f);

				//draw GEMS download link
				DrawCredit(linkFont, "(downloaded in https://opengameart.org/content/gem-icons)", linkSize, GRAY, 7.95f);
				
				//draw author credits
				DrawCredit(menuFont, "Made by Alejandro Insausti", letterSize, GRAY, 8.5f);
			}

			void Draw()
			{
				using namespace config::visuals::menu;

				BeginDrawing();

				//draw all credits
				DrawCredits();

				//draw game ver
				utility::menu::DrawGameVer(menuFont, letterSize, GRAY);
				//draw exit option
				utility::menu::DrawTextbox(menuFont, "Exit", textBoxExit, letterSize * 2, DARKGRAY, GRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void CreditsMenu()
			{
				Init();

				do
				{
					UpdateMusicStream(music::menu);

					Draw();
				} while (!IsKeyReleased(config::keys::keys.quit) && !WindowShouldClose() &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !utility::menu::OptionClicked(textBoxExit)));

				PlaySound(sounds::selectOption);
			}
		}
	}
}