#include "player.h"

#include "Config/audio.h"
#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Utility/utility_calc.h"
#if _DEBUG
#include "Utility/debug_values.h"
#endif // _DEBUG

using namespace endless::utility;
using namespace endless::config;

namespace endless
{
	namespace gameloop
	{
		namespace player
		{
			//EXTERNS
			Player player;
			int highestScore;

			//.CPP ONLY
			enum class JUMP_STATE { RUNNING = 0, JUMPING, FALLING = -1 };

			//constant Vector2 to multiply the screen width or height divided by 10
			const Vector2 playerSize = { 0.5f, 1 }; // [x | y] of screen / 10
			//[x | y] of screen / 10, left lane
			const Vector2 leftPosition = { 1, map::leftLaneYPosition - playerSize.y };
			//[x | y] of screen / 10, mid lane
			const Vector2 midPosition = { 1.3f, map::midLaneYPosition - playerSize.y };
			//[x | y] of screen / 10, right lane
			const Vector2 rightPosition = { 1.6f, map::rightLaneYPosition - playerSize.y };

			const float jumpSpeed = 5.5f;
			const float fallSpeed = jumpSpeed / 3;
			const float jumpMaxHeight = 1.8f; //jump height in player's height (jMH = player.height * x)
			JUMP_STATE jumpState;

			float intangibleCounter;
			const float intagibleDuration = 1.5f;
			const short intagibleCooldown = 4;

			Texture2D playerSprite;

			void InitConfig()
			{
				switch (player.currentLane)
				{
				case MAP_LANES::LEFT:
					player.rec.x = leftPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
					player.rec.y = leftPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::MID:
					player.rec.x = midPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
					player.rec.y = midPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::RIGHT:
					player.rec.x = rightPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
					player.rec.y = rightPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				default:
					break;
				}
				player.rec.width = playerSize.x * (visuals::screenWidth / calc::NumToFloat(10));
				player.rec.height = playerSize.y * (visuals::screenHeight / calc::NumToFloat(10));
			}

			void Init()
			{
				player.currentLane = map::MAP_LANES::MID;
				player.lifes = 3;
				player.points = 0;
				player.isTangible = true;
				InitConfig();

				jumpState = JUMP_STATE::RUNNING;
				intangibleCounter = intagibleCooldown;

				playerSprite = LoadTexture("res/assets/Images/player_ghost_tangible.png");
				playerSprite.height = player.rec.height * 2;
				playerSprite.width = player.rec.width * 2;
			}

			static void Jump()
			{
				float runningPosition = 0;
				switch (player.currentLane)
				{
				case MAP_LANES::LEFT:
					runningPosition = leftPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::MID:
					runningPosition = midPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::RIGHT:
					runningPosition = rightPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				default:
					break;
				}

				switch (jumpState)
				{
				case JUMP_STATE::RUNNING:
					if (IsKeyPressed(keys::keys.jump))
					{
						runningPosition = player.rec.y;
						player.rec.y -= (GetFrameTime() * jumpSpeed) * (visuals::screenHeight / calc::NumToFloat(10));
						jumpState = JUMP_STATE::JUMPING;
					}
					break;
				case JUMP_STATE::JUMPING:
					if (player.rec.y <= static_cast<int>(runningPosition) - (jumpMaxHeight * player.rec.height))
					{
						jumpState = JUMP_STATE::FALLING;
					}
					else
					{
						player.rec.y -= (GetFrameTime() * jumpSpeed) * (visuals::screenHeight / calc::NumToFloat(10));
						player.rec.y += (GetFrameTime() * fallSpeed) * (visuals::screenHeight / calc::NumToFloat(10));
					}
					break;
				case JUMP_STATE::FALLING:
					if (player.rec.y >= static_cast<int>(runningPosition + fallSpeed))
					{
						player.rec.y = runningPosition;
						jumpState = JUMP_STATE::RUNNING;
					}
					else
					{
						player.rec.y += (GetFrameTime() * fallSpeed) * (visuals::screenHeight / calc::NumToFloat(10));
					}
					break;
				default:
					break;
				}
			}

			static float GetPlayerHeight()
			{
				float height = 0;
				switch (player.currentLane)
				{
				case MAP_LANES::LEFT:
					height = player.rec.y - leftPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::MID:
					height = player.rec.y - midPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				case MAP_LANES::RIGHT:
					height = player.rec.y - rightPosition.y * (visuals::screenHeight / calc::NumToFloat(10));
					break;
				default:
					return 0;
					break;
				}
				return height * -1;
			}

			static void ChangeLane()
			{
				if (IsKeyPressed(keys::keys.changeLaneLeft))
				{
					switch (player.currentLane)
					{
					case MAP_LANES::MID:
						player.rec.x = leftPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
						player.rec.y = leftPosition.y * (visuals::screenHeight / calc::NumToFloat(10)) - GetPlayerHeight();
						player.currentLane = MAP_LANES::LEFT;
						break;
					case MAP_LANES::RIGHT:
						player.rec.x = midPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
						player.rec.y = midPosition.y * (visuals::screenHeight / calc::NumToFloat(10)) - GetPlayerHeight();
						player.currentLane = MAP_LANES::MID;
						break;
					default:
						break;
					}
				}
				else if (IsKeyPressed(keys::keys.changeLaneRight))
				{
					switch (player.currentLane)
					{
					case MAP_LANES::LEFT:
						player.rec.x = midPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
						player.rec.y = midPosition.y * (visuals::screenHeight / calc::NumToFloat(10)) - GetPlayerHeight();
						player.currentLane = MAP_LANES::MID;
						break;
					case MAP_LANES::MID:
						player.rec.x = rightPosition.x * (visuals::screenWidth / calc::NumToFloat(10));
						player.rec.y = rightPosition.y * (visuals::screenHeight / calc::NumToFloat(10)) - GetPlayerHeight();
						player.currentLane = MAP_LANES::RIGHT;
						break;
					default:
						break;
					}
				}
			}

			static void BecomeIntangible()
			{
				if (player.isTangible && intangibleCounter >= intagibleCooldown)
				{
					if (IsKeyPressed(keys::keys.dodge))
					{
						PlaySound(audio::sounds::becomeIntangible);
						player.isTangible = false;
						playerSprite = LoadTexture("res/assets/Images/player_ghost_intangible.png");
						playerSprite.height = player.rec.height * 2;
						playerSprite.width = player.rec.width * 2;
						intangibleCounter = 0;
					}
				}
				else
				{
					if (intangibleCounter >= intagibleDuration)
					{
						player.isTangible = true;
						playerSprite = LoadTexture("res/assets/Images/player_ghost_tangible.png");
						playerSprite.height = player.rec.height * 2;
						playerSprite.width = player.rec.width * 2;
					}

					intangibleCounter += GetFrameTime();
					if (intangibleCounter >= intagibleCooldown)
					{
						PlaySound(audio::sounds::finishCooldown);
					}
				}
			}

			void Update()
			{
				Jump();
				BecomeIntangible();
				ChangeLane();
			}

			void Draw()
			{
				if (true)
				{
					DrawTexture(playerSprite, player.rec.x - player.rec.width / 1.25f, player.rec.y - player.rec.height / 2, WHITE);
				}
#if _DEBUG
				if (player.isTangible)
				{
					DrawRectangleLinesEx(player.rec, static_cast<int>(debug_values::lineThick * (visuals::screenWidth / calc::NumToFloat(100))), PURPLE);
				}
				else
				{
					DrawRectangleLinesEx(player.rec, static_cast<int>(debug_values::lineThick * (visuals::screenWidth / calc::NumToFloat(100))), DARKPURPLE);
				}
#endif // _DEBUG
			}

			void DeInit()
			{
				if (player.points > highestScore)
				{
					highestScore = player.points;
				}
			}
		}
	}
}