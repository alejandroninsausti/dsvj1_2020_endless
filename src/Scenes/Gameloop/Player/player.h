#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#include "Scenes/Gameloop/Map/map.h"

using namespace endless::gameloop::map;

namespace endless
{
	namespace gameloop
	{
		namespace player
		{
			struct Player
			{
				Rectangle rec;
				short lifes;
				int points;
				map::MAP_LANES currentLane;
				bool isTangible;
			};

			extern Player player;
			extern int highestScore;

			void InitConfig();
			void Init();
			void Update();
			void Draw();
			void DeInit();
		}
	}
}

#endif // !PLAYER_H