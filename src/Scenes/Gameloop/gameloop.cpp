#include "gameloop.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Config/key_bindings.h"
#include "Config/visuals.h"
#include "Utility/utility_menu.h"
#include "Utility/utility_calc.h"
#include "Scenes/Gameloop/Player/player.h"
#include "Scenes/Gameloop/Map/map.h"
#include "Scenes/Gameloop/Obstacles/objects.h"
#include "Scenes/Menu/Settings/menu_settings.h"

namespace endless
{
	namespace gameloop
	{
		bool pause;
		bool quit;

		Rectangle pauseTextbox;
		Rectangle resumeTextbox;
		Rectangle optionsTextbox;
		Rectangle quitTextbox;

		Texture2D pauseFrame;

		float letterSize;
		float pauseSize;

		static void InitConfigVariables()
		{
			using namespace utility::calc;

			letterSize = NumToFloat(config::visuals::screenWidth) * 0.04f;
			pauseSize = letterSize * 3;

			pauseTextbox = utility::menu::InitTextbox(pauseSize, 6, 5, 2);
			resumeTextbox = utility::menu::InitTextbox(letterSize, 8, 5, 4.5f);
			optionsTextbox = utility::menu::InitTextbox(letterSize, 7, 5, 5.5f);
			quitTextbox = utility::menu::InitTextbox(letterSize, 17, 5, 6.5f);

			pauseFrame = config::visuals::whiteFilter;
			pauseFrame.height = NumToInt(quitTextbox.y + quitTextbox.height - pauseTextbox.y);
			pauseFrame.width = NumToInt(pauseTextbox.width * 1.4f);

			config::visuals::gameloop::Init();
			config::audio::InitGameloop();

			player::InitConfig();
			objects::InitConfig();
		}

		static void Init()
		{
			InitConfigVariables();
			player::Init();
			objects::Init();

			quit = false;
			pause = false;
		}

		static void DeInitConfigVariables()
		{
			config::visuals::gameloop::DeInit();
			config::audio::DeInitGameloop();
		}

		static void DeInit()
		{
			DeInitConfigVariables();

			player::DeInit();
		}

		static void Update()
		{
			UpdateMusicStream(config::audio::music::gameloop);
			if (pause)
			{
				using namespace utility::menu;

				quit = IsKeyReleased(config::keys::keys.quit);

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					if (OptionClicked(resumeTextbox))
					{
						if (player::player.lifes <= 0)
						{
							Init();
						}
						else
						{
							pause = false;
						}
					}
					if (OptionClicked(optionsTextbox))
					{
						DeInitConfigVariables();
						config::audio::InitMenu();
						config::visuals::menu::Init();
						menu::menu_settings::SettingsMenu();
						config::audio::DeInitMenu();
						config::visuals::menu::DeInit();
						InitConfigVariables();
					}
					else if (OptionClicked(quitTextbox))
					{
						quit = true;
					}
				}
			}
			else
			{
				player::Update();
				objects::Update();

				pause = IsKeyReleased(config::keys::keys.quit) || player::player.lifes <= 0;
			}

		}

		static void Draw()
		{
			using namespace config::visuals::menu;
			using namespace utility::menu;
			using namespace utility::calc;

			BeginDrawing();
			//draw Background
			DrawTexture(config::visuals::background, 0, 0, WHITE);
			DrawTextOnly(menuFont, "KEYS: ARROWS to move, SPACE to dodge ASTEROIDS, AVOID SHIPS", letterSize * 0.7f, WHITE, 4.75f, 0.5f);
			DrawTextOnly(menuFont, "ESC to pause", letterSize * 0.7f, WHITE, 4.75f, 1);

			map::Draw();
			player::Draw();
			objects::Draw();
			DrawTextOnly(config::visuals::gameloop::gameloopFont,
				TextFormat("SCORE: %i", player::player.points), letterSize, PURPLE, 8, 2);
			DrawTextOnly(config::visuals::gameloop::gameloopFont,
				TextFormat("LIFES: %i", player::player.lifes), letterSize, PURPLE, 1.5f, 2);

			if (pause)
			{
				DrawTexture(pauseFrame, NumToInt(quitTextbox.x * 0.6f), NumToInt(pauseTextbox.y), WHITE);

				if (player::player.lifes <= 0)
				{
					DrawTextOnly(menuFont, "Game Over", pauseSize, WHITE, 5, 2);
					DrawTextbox(menuFont, "Restart", resumeTextbox, letterSize, WHITE, DARKGRAY);
				}
				else
				{
					DrawTextOnly(menuFont, "Pause", pauseSize, WHITE, 5, 2);
					DrawTextbox(menuFont, "Resume", resumeTextbox, letterSize, WHITE, DARKGRAY);
				}

				DrawTextbox(menuFont, "Options", optionsTextbox, letterSize, WHITE, DARKGRAY);
				DrawTextbox(menuFont, "Go Back To Menu", quitTextbox, letterSize, WHITE, DARKGRAY);
			}
			ClearBackground(GRAY);
			EndDrawing();
		}

		void Gameloop()
		{
			Init();

			do
			{
				Update();
				Draw();
			} while (!quit && !WindowShouldClose());

			DeInit();
		}
	}
}