#include "map.h"

#include "raylib.h"

#include "Config/visuals.h"
#include "Utility/utility_calc.h"
#if _DEBUG
#include "Utility/debug_values.h"
#endif // _DEBUG
using namespace endless::utility;


namespace endless
{
	namespace gameloop
	{
		namespace map
		{
			void Draw()
			{
#if _DEBUG
				using namespace config::visuals;
				using namespace calc;

				Vector2 leftLaneStart;
				leftLaneStart.x = 0;
				leftLaneStart.y = leftLaneYPosition * (screenHeight / NumToFloat(10));
				Vector2 leftLaneEnd;
				leftLaneEnd.x = NumToFloat(screenWidth);
				leftLaneEnd.y = leftLaneYPosition * (screenHeight / NumToFloat(10));
				Vector2 midLaneStart;
				midLaneStart.x = 0;
				midLaneStart.y = midLaneYPosition * (screenHeight / NumToFloat(10));
				Vector2 midLaneEnd;
				midLaneEnd.x = NumToFloat(screenWidth);
				midLaneEnd.y = midLaneYPosition * (screenHeight / NumToFloat(10));
				Vector2 rightLaneStart;
				rightLaneStart.x = 0;
				rightLaneStart.y = rightLaneYPosition * (screenHeight / NumToFloat(10));
				Vector2 rightLaneEnd;
				rightLaneEnd.x = NumToFloat(screenWidth);
				rightLaneEnd.y = rightLaneYPosition * (screenHeight / NumToFloat(10));

				DrawLineEx(leftLaneStart, leftLaneEnd, debug_values::lineThick * (screenWidth / NumToFloat(100)), ORANGE);
				DrawLineEx(midLaneStart, midLaneEnd, debug_values::lineThick * (screenWidth / NumToFloat(100)), ORANGE);
				DrawLineEx(rightLaneStart, rightLaneEnd, debug_values::lineThick * (screenWidth / NumToFloat(100)), ORANGE);
#endif // _DEBUG
			}
		}
	}
}