#ifndef MAP_H
#define MAP_H

namespace endless
{
	namespace gameloop
	{
		namespace map
		{
			enum class MAP_LANES { LEFT = 1, MID, RIGHT };

			const float leftLaneYPosition = 5;
			const float midLaneYPosition = 6.5f;
			const float rightLaneYPosition = 8;

			void Draw();
		}
	}
}

#endif // !MAP_H
