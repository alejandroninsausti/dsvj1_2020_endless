#include "objects.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Config/visuals.h"
#include "Scenes/Gameloop/Map/map.h"
#include "Scenes/Gameloop/Player/player.h"
#include "Utility/utility_calc.h"
#if _DEBUG
#include "Utility/debug_values.h"
#include <iostream>
#endif // _DEBUG
using namespace endless::utility;

using namespace endless::config::visuals;
using namespace endless::config::audio::sounds;
using namespace endless::gameloop::map;

namespace endless
{
	namespace gameloop
	{
		namespace objects
		{
			enum class OBJECT_TYPE { PICKUP_1, PICKUP_5, PICKUP_10, JUMPABLE, DODGEABLE, UNAVOIDABLE };

			struct Object
			{
				Rectangle rec;
				OBJECT_TYPE type;
				MAP_LANES lane;
				bool hasCollided;
			};

			const short lanes = 3;
			const short maxObjectsPerLane = 5;
			short objectsInLane[3];
			Object object[lanes][maxObjectsPerLane];

			Texture2D pickup_1;
			Texture2D pickup_5;
			Texture2D pickup_10;
			Texture2D jumpable;
			Texture2D dodgeable;
			Texture2D unavoidable;

			const float sizePickup = 0.25f;
			const float sizeJumpable = 0.8f;
			const float sizeDodgeable = 2;
			const float sizeUnavoidable = 3;

			void InitConfig()
			{
				pickup_1 = LoadTexture("res/assets/Images/object_pickup1_emptygem.png");
				pickup_1.width = sizePickup * (screenWidth / calc::NumToFloat(10));
				pickup_1.height = sizePickup * (screenHeight / calc::NumToFloat(10));
				pickup_5 = LoadTexture("res/assets/Images/object_pickup5_halfgem.png");
				pickup_5.width = sizePickup * (screenWidth / calc::NumToFloat(10));
				pickup_5.height = sizePickup * (screenHeight / calc::NumToFloat(10));
				pickup_10 = LoadTexture("res/assets/Images/object_pickup10_fullgem.png");
				pickup_10.width = sizePickup * (screenWidth / calc::NumToFloat(10));
				pickup_10.height = sizePickup * (screenHeight / calc::NumToFloat(10));
				jumpable = LoadTexture("res/assets/Images/object_jumpable_shipfire.png");
				jumpable.width = sizeJumpable * (screenWidth / calc::NumToFloat(10));
				jumpable.height = sizeJumpable * (screenHeight / calc::NumToFloat(10));
				dodgeable = LoadTexture("res/assets/Images/object_dodgeable_asteroid.png");
				dodgeable.width = sizeDodgeable * (screenWidth / calc::NumToFloat(10));
				dodgeable.height = sizeDodgeable * (screenHeight / calc::NumToFloat(10));
				unavoidable = LoadTexture("res/assets/Images/object_unavoidable_huntersship.png");
				unavoidable.width = sizeUnavoidable * (screenWidth / calc::NumToFloat(10));
				unavoidable.height = sizeUnavoidable * (screenHeight / calc::NumToFloat(10));

				for (short lane = 0; lane < lanes; lane++)
				{
					for (short i = 0; i < objectsInLane[lane]; i++)
					{
						switch (lane)
						{
						case 0:							
							object[lane][i].rec.y = map::leftLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object[lane][i].rec.height;
							break;
						case 1:
							object[lane][i].rec.y = map::midLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object[lane][i].rec.height;
							break;
						case 2:
							object[lane][i].rec.y = map::rightLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object[lane][i].rec.height;
							break;
						default:
							break;
						}
					}
				}
			}

			void Init()
			{
				for (short i = 0; i < lanes; i++)
				{
					objectsInLane[i] = 0;
				}

				InitConfig();
			}
			
			Object SetNew(OBJECT_TYPE type, short lane)
			{
				Object object;

				object.hasCollided = false;
				object.type = type; //set type

				//set size (fix position in case of 'pickup'
				switch (type)
				{
				case OBJECT_TYPE::PICKUP_1:
				case OBJECT_TYPE::PICKUP_5:
				case OBJECT_TYPE::PICKUP_10:
					object.rec.width = sizePickup * (screenWidth / calc::NumToFloat(10));
					object.rec.height = sizePickup * (screenHeight / calc::NumToFloat(10));
					break;
				case OBJECT_TYPE::JUMPABLE:
					object.rec.width = sizeJumpable * (screenWidth / calc::NumToFloat(10));
					object.rec.height = sizeJumpable * (screenHeight / calc::NumToFloat(10));
					break;
				case OBJECT_TYPE::DODGEABLE:
					object.rec.width = sizeDodgeable / 2 * (screenWidth / calc::NumToFloat(10));
					object.rec.height = sizeDodgeable * (screenHeight / calc::NumToFloat(10));
					break;
				case OBJECT_TYPE::UNAVOIDABLE:
					object.rec.width = sizeUnavoidable * (screenWidth / calc::NumToFloat(10));
					object.rec.height = sizeUnavoidable / 1.05f * (screenHeight / calc::NumToFloat(10));
					break;
				default:
					break;
				}

				//set position and lane
				object.rec.x = calc::NumToFloat(screenWidth);
				switch (lane)
				{
				case 0:
					object.lane = MAP_LANES::LEFT;
					object.rec.y = map::leftLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object.rec.height;
					break;
				case 1:
					object.lane = MAP_LANES::MID;
					object.rec.y = map::midLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object.rec.height;
					break;
				case 2:
					object.lane = MAP_LANES::RIGHT;
					object.rec.y = map::rightLaneYPosition * (screenHeight / calc::NumToFloat(10)) - object.rec.height;
					break;
				default:
					break;
				}

				//fix pickups position
				switch (type)
				{
				case OBJECT_TYPE::PICKUP_1:
				case OBJECT_TYPE::PICKUP_5:
				case OBJECT_TYPE::PICKUP_10:
					object.rec.y -= object.rec.height * 1.5f;
				default:
					break;
				}

				return object;
			}

			const short chanceOfNewObject = 5;
			const short chanceOfNewPickup_1 = 20; //total 20
			const short chanceOfNewPickup_5 = chanceOfNewPickup_1 + 15; //total 35
			const short chanceOfNewPickup_10 = chanceOfNewPickup_5 + 5; //total 40
			const short chanceOfNewJumpable = chanceOfNewPickup_10 + 30; //total 70
			const short chanceOfNewDodgeable = chanceOfNewJumpable + 20; //total 90
			const short chanceOfNewUnavoidable = chanceOfNewUnavoidable + 10; //total 100
			const float distanceBetweenObjects = 8;
			void Generator(short lane)
			{
				if (GetRandomValue(1, 100) <= chanceOfNewObject && objectsInLane[lane] < maxObjectsPerLane)
				{
					OBJECT_TYPE newType;
					short typeRandomizer = GetRandomValue(1, 100);
					if (typeRandomizer <= chanceOfNewPickup_1)
					{
						newType = OBJECT_TYPE::PICKUP_1;
					}
					else if (typeRandomizer <= chanceOfNewPickup_5)
					{
						newType = OBJECT_TYPE::PICKUP_5;
					}
					else if (typeRandomizer <= chanceOfNewPickup_10)
					{
						newType = OBJECT_TYPE::PICKUP_10;
					}
					else if (typeRandomizer <= chanceOfNewJumpable)
					{
						newType = OBJECT_TYPE::JUMPABLE;
					}
					else if (typeRandomizer <= chanceOfNewDodgeable)
					{
						newType = OBJECT_TYPE::DODGEABLE;
					}
					else
					{
						newType = OBJECT_TYPE::UNAVOIDABLE;
					}

					if (objectsInLane[lane] == 0)
					{
						object[lane][0] = SetNew(newType, lane);
						objectsInLane[lane]++;
					}
					else
					{
						if (object[lane][objectsInLane[lane] - 1].rec.x + object[lane][objectsInLane[lane] - 1].rec.width
							<= (screenWidth / calc::NumToFloat(10)) * distanceBetweenObjects)
						{
							object[lane][objectsInLane[lane]] = SetNew(newType, lane);
							objectsInLane[lane]++;
						}
					}
				}
			}

			void Destroy(short lane, short objectIndex)
			{
				for (short j = objectIndex; j < objectsInLane[lane] - 1; j++)
				{
					object[lane][j] = object[lane][j + 1];
				}
				objectsInLane[lane]--;
			}

			void CollideWithPlayer(short lane, short objectIndex)
			{
				if (CheckCollisionRecs(player::player.rec, object[lane][objectIndex].rec))
				{
					switch (object[lane][objectIndex].type)
					{
					case OBJECT_TYPE::PICKUP_1:
						PlaySound(pickup);						
						player::player.points += 1;
						Destroy(lane, objectIndex);
						break;
					case OBJECT_TYPE::PICKUP_5:
						PlaySound(pickup);						
						player::player.points += 5;
						Destroy(lane, objectIndex);
						break;
					case OBJECT_TYPE::PICKUP_10:
						PlaySound(pickup);						
						player::player.points += 10;
						Destroy(lane, objectIndex);
						break;
					case OBJECT_TYPE::DODGEABLE:
						if (!player::player.isTangible)
						{
							break;
						}
					default:
						if (!object[lane][objectIndex].hasCollided)
						{
							PlaySound(playerHitted);
							object[lane][objectIndex].hasCollided = true;
							player::player.lifes--;
						}
						break;
					}

#if _DEBUG
					std::cout
						<< "L:" << player::player.lifes
						<< " P:" << player::player.points
						<< " T:" << player::player.isTangible
						<< std::endl;
#endif // _DEBUG
				}
			}

			const float objectSpeed = 2.5f;
			void Move(short lane)
			{
				for (short i = 0; i < objectsInLane[lane]; i++)
				{
					float frameTime = GetFrameTime();
					object[lane][i].rec.x -= (frameTime * objectSpeed) * (screenWidth / calc::NumToFloat(10));

					if (object[lane][i].lane == player::player.currentLane)
					{
						CollideWithPlayer(lane, i);
					}
				}
			}

			void DestroyOutOfMap(short lane)
			{
				for (short i = 0; i < objectsInLane[lane]; i++)
				{
					if (object[lane][i].rec.x + object[lane][i].rec.width <= 0)
					{
						Destroy(lane, i);
					}
				}
			}

			void Update()
			{
				Generator(GetRandomValue(0, 2)); //generate new object in a random lane

				//update every object on map
				for (short i = 0; i < lanes; i++)
				{
					Move(i);
					DestroyOutOfMap(i);
				}
			}

			void Draw()
			{
				for (short i = 0; i < lanes; i++)
				{
					for (short j = 0; j < objectsInLane[i]; j++)
					{
						switch (object[i][j].type)
						{
						case OBJECT_TYPE::PICKUP_1:
							DrawTexture(pickup_1, object[i][j].rec.x, object[i][j].rec.y, WHITE);
							break;
						case OBJECT_TYPE::PICKUP_5:
							DrawTexture(pickup_5, object[i][j].rec.x, object[i][j].rec.y, WHITE);
							break;
						case OBJECT_TYPE::PICKUP_10:
							DrawTexture(pickup_10, object[i][j].rec.x, object[i][j].rec.y, WHITE);
							break;
						case OBJECT_TYPE::JUMPABLE:
							DrawTexture(jumpable, object[i][j].rec.x, object[i][j].rec.y, WHITE);
							break;
						case OBJECT_TYPE::DODGEABLE:
							DrawTexture(dodgeable, object[i][j].rec.x, object[i][j].rec.y, WHITE);
							break;
						case OBJECT_TYPE::UNAVOIDABLE:
							DrawTexture(unavoidable, object[i][j].rec.x, object[i][j].rec.y + object[i][j].rec.y * 0.1f, WHITE);
							break;
						default:
							break;
						}
#if _DEBUG
						//set color
						Color color = GRAY;
						switch (object[i][j].type)
						{
						case OBJECT_TYPE::PICKUP_1:
							color = YELLOW;
							break;
						case OBJECT_TYPE::PICKUP_5:
							color = ORANGE;
							break;
						case OBJECT_TYPE::PICKUP_10:
							color = BROWN;
							break;
						case OBJECT_TYPE::JUMPABLE:
							color = RED;
							break;
						case OBJECT_TYPE::DODGEABLE:
							color = SKYBLUE;
							break;
						case OBJECT_TYPE::UNAVOIDABLE:
							color = BLACK;
							break;
						default:
							break;
						}
						//draw
						DrawRectangleLinesEx(object[i][j].rec, calc::NumToInt(debug_values::lineThick * (screenWidth / calc::NumToFloat(100))), color);
#endif // _DEBUG
					}
				}
			}
		}
	}
}
