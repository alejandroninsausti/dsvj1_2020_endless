#ifndef OBJECTS_H
#define OBJECTS_H

namespace endless
{
	namespace gameloop
	{		
		namespace objects
		{
			void InitConfig();
			void Init();
			void Update();
			void Draw();
		}
	}
}

#endif // !OBJECTS_H
