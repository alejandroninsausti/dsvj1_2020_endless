#include "game.h"

#include "raylib.h"

#include "Config/audio.h"
#include "Config/visuals.h"
#include "Config/key_bindings.h"
#include "Scenes/Menu/menu.h"

namespace endless
{
	void Init()
	{
		using namespace config;

		visuals::Init();
		audio::Init();
		keys::Init();
	}

	void DeInit()
	{
		CloseWindow(); //close Game
	}

	void StartGame()
	{
		Init();
		menu::menu_main::MainMenu();
		DeInit();
	}
}