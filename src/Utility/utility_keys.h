#ifndef UTILITY_KEYS_H
#define UTILITY_KEYS_H

namespace endless
{
	namespace utility
	{
		namespace keys
		{
			int InputKey();
		}
	}
}

#endif // !UTILITY_KEYS_H