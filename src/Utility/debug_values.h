#ifndef DEBUG_VALUABLES_H
#define DEBUG_VALUABLES_H

namespace endless
{
	namespace utility
	{
		namespace debug_values
		{
			const float lineThick = 0.5f;
		}
	}
}

#endif // !DEBUG_VALUABLES_H
