#ifndef VISUALS_H
#define VISUALS_H

#include "raylib.h"

namespace endless
{
	namespace config
	{
		namespace visuals
		{
			enum class RESOLUTIONS { LOW_RES, MID_RES, HIGH_RES };

			extern RESOLUTIONS res;
			extern int screenWidth;
			extern int screenHeight;

			extern Texture2D background;
			extern Texture2D whiteFilter;

			namespace menu
			{
				//----------------fonts-----------------
				extern Font menuFont;
				extern Font linkFont;

				void Init();
				void DeInit();
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				extern Font gameloopFont;
				//--------------textures----------------

				void Init();
				void DeInit();
			}

			void InitScreenSize(RESOLUTIONS newRes);
			void Init();
		}
	}
}

#endif // !VISUALS_H
