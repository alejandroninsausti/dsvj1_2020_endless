#ifndef VOLUME_H
#define VOLUME_H

#include "raylib.h"

namespace endless
{
	namespace config
	{
		namespace audio
		{
			extern float masterVolume;
			extern float musicVolume;
			extern float soundVolume;

			namespace sounds
			{
				extern Sound selectOption;
				extern Sound becomeIntangible;
				extern Sound finishCooldown;
				extern Sound pickup;
				extern Sound playerHitted;

				void Update();
			}

			namespace music
			{
				extern Music menu;
				extern Music gameloop;

				void Update();
			}

			void InitMenu();
			void InitGameloop();
			void Init();
			void DeInitMenu();
			void DeInitGameloop();
		}
	}
}

#endif // !VOLUME_H