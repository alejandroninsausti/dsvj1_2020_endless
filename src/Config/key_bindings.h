#ifndef KEY_BINDINGS_H
#define KEY_BINDINGS_H

namespace endless
{
	namespace config
	{
		namespace keys
		{
			struct KeyBindings
			{
				int changeLaneLeft;
				int changeLaneRight;
				int jump;
				int dodge;
				int quit;
			};

			void Init();

			extern KeyBindings keys;
		}
	}
}

#endif // !KEY_BINDINGS_H
