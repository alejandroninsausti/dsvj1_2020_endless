#include "visuals.h"

#include "Utility/utility_menu.h"

namespace endless
{
	namespace config
	{
		namespace visuals
		{
			RESOLUTIONS res;
			int screenWidth;
			int screenHeight;

			Texture2D background;
			Texture2D whiteFilter;

			namespace menu
			{
				//----------------fonts-----------------
				Font menuFont;
				Font linkFont;


				void Init()
				{
					menuFont = GetFontDefault();
					linkFont = menuFont;
				}

				void DeInit()
				{
					UnloadFont(menuFont);
					UnloadFont(linkFont);
				}
			}

			namespace gameloop
			{
				//----------------fonts-----------------
				Font gameloopFont;
				//--------------textures----------------

				void Init()
				{
					gameloopFont = menu::menuFont;
					background.height = screenHeight;
					background.width = screenWidth;
				}

				void DeInit()
				{
					UnloadFont(gameloopFont);
				}
			}

			void InitScreenSize(RESOLUTIONS newRes)
			{
				res = newRes;

				switch (res)
				{
				case RESOLUTIONS::LOW_RES:
					screenWidth = 720;
					screenHeight = 480;
					break;
				case RESOLUTIONS::MID_RES:
					screenWidth = 800;
					screenHeight = 600;
					break;
				case RESOLUTIONS::HIGH_RES:
					screenWidth = 1152;
					screenHeight = 900;
					break;
				default:
					break;
				}

				SetWindowSize(screenWidth, screenHeight);
			}

			void Init()
			{
				InitScreenSize(RESOLUTIONS::MID_RES);

				InitWindow(visuals::screenWidth, visuals::screenHeight, TextFormat("Basic Endless %s", utility::menu::gameVersion)); //open OpenGL window
				SetTargetFPS(60); //set max frame rate

				background = LoadTexture("res/assets/Images/background_space.png");
				whiteFilter = LoadTexture("res/assets/Images/white_filter.png");

				menu::Init();
			}
		}
	}
}