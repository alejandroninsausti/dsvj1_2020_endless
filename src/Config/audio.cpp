#include "audio.h"

namespace endless
{
	namespace config
	{
		namespace audio
		{
			float masterVolume = 1;
			float musicVolume = 1;
			float soundVolume = 1;
						
			namespace sounds
			{
				Sound selectOption;
				Sound becomeIntangible;
				Sound finishCooldown;
				Sound pickup;
				Sound playerHitted;

				void InitMenu()
				{
					selectOption = LoadSound("res/assets/SFX/select_sound.mp3");
				}

				void InitGameloop()
				{
					becomeIntangible = LoadSound("res/assets/SFX/become_intangible_sound.mp3");
					finishCooldown = LoadSound("res/assets/SFX/finish_cooldown_sound.mp3");
					pickup = LoadSound("res/assets/SFX/pickup_sound.mp3");
					playerHitted = LoadSound("res/assets/SFX/player_hitted_sound.mp3");
				}

				void Update()
				{
					SetSoundVolume(selectOption, soundVolume);
					SetSoundVolume(becomeIntangible, soundVolume);
					SetSoundVolume(finishCooldown, soundVolume);
					SetSoundVolume(pickup, soundVolume);
					SetSoundVolume(playerHitted, soundVolume);
				}

				void DeInitMenu()
				{
					UnloadSound(selectOption);
				}

				void DeInitGameloop()
				{
					UnloadSound(becomeIntangible);
					UnloadSound(finishCooldown);
					UnloadSound(pickup);
					UnloadSound(playerHitted);
				}
			}

			namespace music
			{
				Music menu;
				Music gameloop;

				void InitMenu()
				{
					if (!IsMusicPlaying(menu))
					{
						menu = LoadMusicStream("res/assets/Music/menu_ost.mp3");
						PlayMusicStream(menu);
					}
				}

				void InitGameloop()
				{
					if (!IsMusicPlaying(gameloop))
					{
						gameloop = LoadMusicStream("res/assets/Music/space_ost.mp3");
						PlayMusicStream(gameloop);
					}
				}

				void Update()
				{
					SetMusicVolume(menu, musicVolume);
					SetMusicVolume(gameloop, musicVolume);
				}

				void DeInitMenu()
				{
					StopMusicStream(menu);
					UnloadMusicStream(menu);
				}

				void DeInitGameloop()
				{
					StopMusicStream(gameloop);
					UnloadMusicStream(gameloop);
				}
			}

			void InitMenu()
			{
				sounds::InitMenu();
				music::InitMenu();

				music::Update();
				sounds::Update();
			}

			void InitGameloop()
			{
				sounds::InitGameloop();
				music::InitGameloop();

				music::Update();
				sounds::Update();
			}

			void Init()
			{
				InitAudioDevice(); //init audio device
				SetMasterVolume(0.5f); //set initial volume	

				InitMenu();
			}

			void DeInitMenu()
			{
				sounds::DeInitMenu();
				music::DeInitMenu();
			}

			void DeInitGameloop()
			{
				sounds::DeInitGameloop();
				music::DeInitGameloop();
			}
		}
	}
}