#include "key_bindings.h"

#include "raylib.h"

namespace endless
{
	namespace config
	{
		namespace keys
		{
			KeyBindings keys;

			void Init()
			{
				SetExitKey(0); //set raylib exit key to NULL

				keys.changeLaneLeft = KEY_LEFT;
				keys.changeLaneRight = KEY_RIGHT;
				keys.jump = KEY_UP;
				keys.dodge = KEY_SPACE;
				keys.quit = KEY_ESCAPE;
			}
		}
	}
}